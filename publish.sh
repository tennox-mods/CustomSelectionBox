#!/usr/bin/env fish

set mod_id (grep modrinth_id gradle.properties | choose -f '=' 1) || return 1
echo "Mod ID: $mod_id"

if not set -q MODRINTH_TOKEN
  echo "Missing env: MODRINTH_TOKEN" >&2
  exit 1
end

gradle fabric:publishUnified

echo 'Updating README...'
set body_file (mktemp) || return 1
grep -v '# Custom Selection Box' README.md > $body_file || return 1
head $body_file
http -q PATCH https://api.modrinth.com/v2/project/$mod_id Authorization:$MODRINTH_TOKEN "body=@$body_file" || return 1

echo -e "\nCheck it out: https://modrinth.com/mod/custom-selection-box"