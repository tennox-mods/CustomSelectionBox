### Dev Setup
0. (optional) `direnv allow` (sets up all dependencies, but requires [nix](https://nixos.org/download.html))
1. Follow instructions on [fabric wiki](https://fabricmc.net/wiki/tutorial:setup)
2. `gradle quilt:runClient`

### Update Minecraft version and dependencies
1. Update versions in `gradle.properties` (see comments in there)
2. Update dependencies in `{fabric-like,fabric,quilt}/build.gradle` (I marked all locations to check with `// UPDATE`)
3. Perform a Gradle sync (if using IDEA)
4. Test the game

### Publish new version
1. Update `mod_version` in `gradle.properties`
2. Set `MODRINTH_TOKEN` to a valid API token 
3. Run `./publish.sh`