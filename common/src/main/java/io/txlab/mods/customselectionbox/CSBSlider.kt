package io.txlab.mods.customselectionbox

import com.mojang.blaze3d.systems.RenderSystem
import com.mojang.blaze3d.vertex.PoseStack
import io.txlab.mods.customselectionbox.CustomSelectionBox.LOGGER
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.components.AbstractSliderButton
import net.minecraft.client.renderer.GameRenderer
import net.minecraft.network.chat.CommonComponents
import net.minecraft.network.chat.Component
import net.minecraft.resources.ResourceLocation
import net.minecraft.util.Mth

val SMALL_WIDGETS_LOCATION = ResourceLocation("customselectionbox:textures/widgets.png")

class CSBSlider(
    private val field: String, x: Int, y: Int
) : AbstractSliderButton(
    x, y, 150, 15, CommonComponents.EMPTY, getValFromConfig(field)
) {
    init {
        updateMessage()
    }

    private fun getDisplayString(): String {
        val translated = Component.translatable("customselectionbox.options.${this.field}").getString()
        return when (this.field) {
            "red" -> "$translated: " + Math.round(value * 255.0f)
            "green" -> "$translated: " + Math.round(value * 255.0f)
            "blue" -> "$translated: " + Math.round(value * 255.0f)
            "alpha" -> "$translated: " + Math.round(value * 255.0f)
            "thickness" -> "$translated: " + Math.round(value * 7.0f)
            "blinkAlpha" -> "$translated: " + Math.round(value * 255.0f)
            "blockAlpha" -> "$translated: " + Math.round(value * 255.0f)
            "blinkSpeed" -> "$translated: " + Math.round(value * 100.0f)
            else -> "invalid option?! (${this.field})"
        }
    }

    override fun updateMessage() {
        message = Component.literal(getDisplayString())
    }

    override fun applyValue() {
        val config = CustomSelectionBox.CONFIG
        LOGGER.info("Setting value {} from {} to {}", this.field, getValFromConfig(this.field), this.value)
        when (this.field) {
            "red" -> config.red = value.toFloat()
            "green" -> config.green = value.toFloat()
            "blue" -> config.blue = value.toFloat()
            "alpha" -> config.alpha = value.toFloat()
            "thickness" -> config.thickness = value.toFloat() * 7.0f
            "blinkAlpha" -> config.blinkAlpha = value.toFloat()
            "blockAlpha" -> config.blockAlpha = value.toFloat()
            "blinkSpeed" -> config.blinkSpeed = value.toFloat()
            else -> throw Error("invalid option: ${this.field}")
        }
    }

    /*
     * Overrides to support smaller height
     */
    override fun renderBg(poseStack: PoseStack, minecraft: Minecraft, i: Int, j: Int) {
        RenderSystem.setShaderTexture(0, SMALL_WIDGETS_LOCATION)
        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f)
        val k = (if (this.isHoveredOrFocused) 2 else 1) * 20
        this.blit(poseStack, x + (value * (width - 8).toDouble()).toInt(), y, 0, 46 + k, 4, this.height)
        this.blit(poseStack, x + (value * (width - 8).toDouble()).toInt() + 4, y, 196, 46 + k, 4, this.height)
    }

    override fun renderButton(poseStack: PoseStack, i: Int, j: Int, f: Float) {
        val minecraft = Minecraft.getInstance()
        val font = minecraft.font
        RenderSystem.setShader { GameRenderer.getPositionTexShader() }
        RenderSystem.setShaderTexture(0, SMALL_WIDGETS_LOCATION)
        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, alpha)
        val k = getYImage(this.isHoveredOrFocused)
        RenderSystem.enableBlend()
        RenderSystem.defaultBlendFunc()
        RenderSystem.enableDepthTest()
        this.blit(poseStack, x, y, 0, 46 + k * 20, width / 2, height)
        this.blit(poseStack, x + width / 2, y, 200 - width / 2, 46 + k * 20, width / 2, height)
        renderBg(poseStack, minecraft, i, j)
        val l = if (active) 16777215 else 10526880
        drawCenteredString(
            poseStack, font,
            message, x + width / 2, y + (height - 8) / 2 + 1, l or (Mth.ceil(alpha * 255.0f) shl 24)
        )
    }
}

private fun getValFromConfig(field: String) = (when (field) {
    "red" -> CustomSelectionBox.CONFIG.red
    "green" -> CustomSelectionBox.CONFIG.green
    "blue" -> CustomSelectionBox.CONFIG.blue
    "alpha" -> CustomSelectionBox.CONFIG.alpha
    "thickness" -> CustomSelectionBox.CONFIG.thickness / 7.0f
    "blinkAlpha" -> CustomSelectionBox.CONFIG.blinkAlpha
    "blockAlpha" -> CustomSelectionBox.CONFIG.blockAlpha
    "blinkSpeed" -> CustomSelectionBox.CONFIG.blinkSpeed
    else -> throw Error("invalid option: $field")
}).toDouble()