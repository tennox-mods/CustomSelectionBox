package io.txlab.mods.customselectionbox.mixin;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.*;
import io.txlab.mods.customselectionbox.CSBConfig;
import io.txlab.mods.customselectionbox.CustomSelectionBox;
import io.txlab.mods.customselectionbox.api.CSBRenderer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.*;
import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import org.lwjgl.opengl.GL11;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Objects;
import java.util.OptionalDouble;

import static io.txlab.mods.customselectionbox.CustomSelectionBox.LOGGER;
import static io.txlab.mods.customselectionbox.CustomSelectionBox.RENDERERS;
import static net.minecraft.client.renderer.RenderStateShard.*;

@Mixin(LevelRenderer.class)
public class LevelRenderMixin {
    private long start = System.currentTimeMillis();
    @Shadow
    private ClientLevel level;

    @Shadow
    @Final
    private Minecraft minecraft;
    @Shadow
    private RenderBuffers renderBuffers;

    @Inject(method = "renderHitOutline", at = @At("HEAD"), cancellable = true)
    public void drawBlockOutline(
            PoseStack poseStack, VertexConsumer vertexConsumer, Entity entity, double camX, double camY, double camZ, BlockPos blockPos, BlockState blockState, CallbackInfo ci
    ) {
        boolean log = Math.random() < 0.0001;
        VoxelShape outlineShape = blockState.getShape(this.level, blockPos, CollisionContext.of(entity));
        if (log) {
            LOGGER.debug("shape: {}, tick={}", outlineShape, this.minecraft.getFrameTime());
        }
        CSBConfig config = CustomSelectionBox.CONFIG;

        MultiBufferSource.BufferSource bufferSource = this.renderBuffers.bufferSource();
        for (CSBRenderer renderer : RENDERERS) {
            boolean result = renderer.render(poseStack, bufferSource, this.level, entity, camX, camY, camZ, blockPos, blockState, config);
            if (log) LOGGER.debug("render result: {}", result);
            if (result) {
                ci.cancel(); // skip default rendering
                break;
            }
        }
    }
}
