package io.txlab.mods.customselectionbox.mixin.accessors;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.Widget;
import net.minecraft.client.gui.components.events.GuiEventListener;
import net.minecraft.client.gui.narration.NarratableEntry;
import net.minecraft.client.gui.screens.Screen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.gen.Invoker;

@SuppressWarnings("UnusedReturnValue")
@Mixin(Screen.class)
public interface ScreenAccessor {
	@Accessor
	int getWidth();
	@Accessor
	int getHeight();

	@Accessor
	Minecraft getMinecraft();
	//@Accessor
	//List<Widget> getRenderables();

	//@Invoker
	//<T extends GuiEventListener & Widget & NarratableEntry> T callAddRenderableWidget(T guiEventListener);
}
