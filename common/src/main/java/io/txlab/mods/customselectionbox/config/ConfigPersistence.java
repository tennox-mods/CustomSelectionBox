package io.txlab.mods.customselectionbox.config;

import dev.architectury.injectables.annotations.ExpectPlatform;
import dev.architectury.platform.Platform;
import io.txlab.mods.customselectionbox.CSBConfig;

import java.nio.file.Path;

public class ConfigPersistence {
    /**
     * We can use {@link Platform#getConfigFolder()} but this is just an example of {@link ExpectPlatform}.
     * <p>
     * This must be a <b>public static</b> method. The platform-implemented solution must be placed under a
     * platform sub-package, with its class suffixed with {@code Impl}.
     * <p>
     * Example:
     * Expect: io.txlab.mods.customselectionbox.ExampleExpectPlatform#getConfigDirectory()
     * Actual Fabric: io.txlab.mods.customselectionbox.fabric.ExampleExpectPlatformImpl#getConfigDirectory()
     * Actual Forge: io.txlab.mods.customselectionbox.forge.ExampleExpectPlatformImpl#getConfigDirectory()
     * <p>
     * <a href="https://plugins.jetbrains.com/plugin/16210-architectury">You should also get the IntelliJ plugin to help with @ExpectPlatform.</a>
     */
    @ExpectPlatform
    public static CSBConfig load() {
        // Just throw an error, the content should get replaced at runtime.
        throw new AssertionError("ConfigPersistence no Impl");
    }
    @ExpectPlatform
    public static void save(CSBConfig config) {
        // Just throw an error, the content should get replaced at runtime.
        throw new AssertionError("ConfigPersistence no Impl");
    }
}
