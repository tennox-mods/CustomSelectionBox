package io.txlab.mods.customselectionbox

class CSBConfig {
    var red = 0.0f
    var green = 0.0f
    var blue = 0.0f
    var alpha = 0.0f
    var thickness = 0.0f
    var blinkAlpha = 0.0f
    var blinkSpeed = 0.0f
    var blockAlpha = 0.0f

    //var diffButtonLoc = false
    var linkBlocks = false
    var disableDepthBuffer = false
    var breakAnimation: BREAK_ANIMATION = BREAK_ANIMATION.NONE

    init {
        defaultsMC()
    }

    fun defaultsCSB() {
        red = 0f
        green = 0f
        blue = 0f
        alpha = 0.4f
        thickness = 4f
        blockAlpha = 0.5f
        blinkAlpha = 0.3f
        blinkSpeed = .2f
        linkBlocks = true
        disableDepthBuffer = true
    }

    fun defaultsMC() {
        red = 0f
        green = 0f
        blue = 0f
        alpha = 0.4f
        thickness = 2.5f
        blockAlpha = 0f
        blinkAlpha = 0f
        blinkSpeed = 0f
        linkBlocks = false
        disableDepthBuffer = false
    }
}

enum class BREAK_ANIMATION {
    NONE,
    SHRINK,
    DOWN,
    ALPHA,
    LASTANIMATION_INDEX
}