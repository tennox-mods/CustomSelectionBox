package io.txlab.mods.customselectionbox.renderer

import com.mojang.blaze3d.systems.RenderSystem
import com.mojang.blaze3d.vertex.*
import io.txlab.mods.customselectionbox.CSBConfig
import io.txlab.mods.customselectionbox.CustomSelectionBox
import io.txlab.mods.customselectionbox.CustomSelectionBox.LOGGER
import io.txlab.mods.customselectionbox.api.CSBRenderer
import net.minecraft.client.Minecraft
import net.minecraft.client.multiplayer.ClientLevel
import net.minecraft.client.renderer.GameRenderer
import net.minecraft.client.renderer.MultiBufferSource
import net.minecraft.client.renderer.RenderStateShard
import net.minecraft.client.renderer.RenderType
import net.minecraft.core.BlockPos
import net.minecraft.core.Direction
import net.minecraft.util.Mth
import net.minecraft.world.entity.Entity
import net.minecraft.world.level.block.*
import net.minecraft.world.level.block.piston.PistonBaseBlock
import net.minecraft.world.level.block.piston.PistonHeadBlock
import net.minecraft.world.level.block.state.BlockState
import net.minecraft.world.level.block.state.properties.BedPart
import net.minecraft.world.level.block.state.properties.PistonType
import net.minecraft.world.phys.AABB
import net.minecraft.world.phys.shapes.BooleanOp
import net.minecraft.world.phys.shapes.CollisionContext
import net.minecraft.world.phys.shapes.Shapes
import net.minecraft.world.phys.shapes.VoxelShape
import org.lwjgl.opengl.GL11
import java.util.*

class CSBDefaultRenderer : CSBRenderer {
    private val start = System.currentTimeMillis()

    override fun getPriority(): Float {
        return 100.0f
    }

    var log = false

    override fun render(
        poseStack: PoseStack,
        bufferSource: MultiBufferSource.BufferSource,
        world: ClientLevel,
        entity: Entity,
        camX: Double,
        camY: Double,
        camZ: Double,
        blockPos: BlockPos,
        blockState: BlockState,
        config: CSBConfig
    ): Boolean {
        log = Math.random() < 0.01
        //if (1 > 2 && Math.sin(System.currentTimeMillis() / 150.0) < 0.5) return false
        GL11.glEnable(GL11.GL_LINE_SMOOTH) // ? not sure if this has any effect atm
        var shape = blockState.getShape(world, blockPos, CollisionContext.of(entity))
        if (CustomSelectionBox.CONFIG.linkBlocks) shape = adjustShapeByLinkedBlocks(world, blockState, blockPos, shape)
        if (log) LOGGER.debug("Shape after adjust: {}", shape)

        if (config.alpha > 0)
            drawOutlinedBoundingBox(poseStack, bufferSource, shape, blockPos, camX, camY, camZ, config)
        if (config.blockAlpha > 0)
            drawBlinkingBlock(poseStack, bufferSource, shape, camX, camY, camZ, blockPos, config)
        GL11.glDisable(GL11.GL_LINE_SMOOTH)

        //Minecraft.getInstance().debugRenderer.solidFaceRenderer.render(poseStack, bufferSource, camX, camY, camZ)
        return true
    }

    fun drawOutlinedBoundingBox(
        poseStack: PoseStack,
        bufferSource: MultiBufferSource.BufferSource,
        voxelShape: VoxelShape,
        blockPos: BlockPos,
        camX: Double,
        camY: Double,
        camZ: Double,
        config: CSBConfig
    ) {
        val builder = RenderType.CompositeState.builder()
        builder.setShaderState(RenderStateShard.RENDERTYPE_LINES_SHADER /*RENDERTYPE_CUTOUT_SHADER*/)
        //builder.setLineState(new LineStateShard(OptionalDouble.empty()));
        val lineThickness = config.thickness.toDouble()
            .coerceAtLeast(Minecraft.getInstance().window.width.toDouble() / 1920.0 * 2.5) // copied from vanilla code
        builder.setLineState(RenderStateShard.LineStateShard(OptionalDouble.of(lineThickness)))
        builder.setLayeringState(RenderStateShard.VIEW_OFFSET_Z_LAYERING)
        builder.setTransparencyState(RenderStateShard.TRANSLUCENT_TRANSPARENCY)
        builder.setOutputState(RenderStateShard.ITEM_ENTITY_TARGET)
        if (config.disableDepthBuffer) builder.setDepthTestState(RenderStateShard.NO_DEPTH_TEST)
        if (!config.disableDepthBuffer) builder.setDepthTestState(RenderStateShard.LEQUAL_DEPTH_TEST)
        //if (config.getDisableDepthBuffer()) builder.setWriteMaskState(COLOR_WRITE);
        builder.setCullState(RenderStateShard.NO_CULL)
        val compositeState = builder.createCompositeState(false)
        val renderType = RenderType.create(
            "csb-lines",
            DefaultVertexFormat.POSITION_COLOR_NORMAL,
            VertexFormat.Mode.LINES,
            256,
            compositeState
        )
        val vertexConsumer = bufferSource.getBuffer(renderType)

        //if (rainbow) {
        //    final double millis = System.currentTimeMillis() % 10000L / 10000.0f;
        //    final int color = HSBtoRGB((float) millis, 0.8f, 0.8f);
        //    r = (color >> 16 & 255) / 255.0f;
        //    g = (color >> 8 & 255) / 255.0f;
        //    b = (color & 255) / 255.0f;
        //}
        poseStack.pushPose()
        val pose = poseStack.last()
        val sin = Math.sin(System.currentTimeMillis() / 70.0 * config.blinkSpeed)
        val blinkingMultiplier: Float = if (config.blinkSpeed > 0) 1 - Math.abs(sin).toFloat() else 1.0f
        val blinkAlpha = config.alpha - config.alpha * config.blinkAlpha * blinkingMultiplier /* 0.3f*/
        val relX = blockPos.x - camX
        val relY = blockPos.y - camY
        val relZ = blockPos.z - camZ
        if (log) LOGGER.debug("[OUT] relX={}, relY={}, relZ={}", relX, relY, relZ)
        var i = 0
        voxelShape.forAllBoxes { boxStartX: Double, boxStartY: Double, boxStartZ: Double, boxEndX: Double, boxEndY: Double, boxEndZ: Double ->
            if (log) LOGGER.debug(
                "[OUT-BOX] x: {}⇾{} y: {}⇾{} z: {}⇾{}",
                boxStartX, boxEndX, boxStartY, boxEndY, boxStartZ, boxEndZ
            )
            val drawLine = { startX: Double, startY: Double, startZ: Double, endX: Double, endY: Double, endZ: Double ->
                // Check if it's part of a face of a different box (e.g. piston shaft has faces that are 'in' the face of head/base blocks)
                var partOfOtherFace = false
                voxelShape.forAllBoxes { otherBoxStartX: Double, otherBoxStartY: Double, otherBoxStartZ: Double, otherBoxEndX: Double, otherBoxEndY: Double, otherBoxEndZ: Double ->
                    if (!((boxStartX == otherBoxStartX && boxStartY == otherBoxStartY && boxStartZ == otherBoxStartZ) && (boxEndX == otherBoxEndX && boxEndY == otherBoxEndY && boxEndZ == otherBoxEndZ))) {
                        //if (log) LOGGER.debug(
                        //    "[OUT-BOX INNER] x: {}⇾{} y: {}⇾{} z: {}⇾{}",
                        //    otherBoxStartX, otherBoxEndX, otherBoxStartY, otherBoxEndY, otherBoxStartZ, otherBoxEndZ
                        //)
                        if (startX == endX && startX == otherBoxEndX && ((startY > otherBoxStartY && endY < otherBoxEndY) || (startZ > otherBoxStartZ && endZ < otherBoxEndZ))) partOfOtherFace = true
                        if (startY == endY && startY == otherBoxEndY && ((startX > otherBoxStartX && endX < otherBoxEndX) || (startZ > otherBoxStartZ && endZ < otherBoxEndZ))) partOfOtherFace = true
                        if (startZ == endZ && startZ == otherBoxEndZ && ((startX > otherBoxStartX && endX < otherBoxEndX) || (startY > otherBoxStartY && endY < otherBoxEndY))) partOfOtherFace = true
                        if (startX == endX && endX == otherBoxStartX && ((startY > otherBoxStartY && endY < otherBoxEndY) || (startZ > otherBoxStartZ && endZ < otherBoxEndZ))) partOfOtherFace = true
                        if (startY == endY && endY == otherBoxStartY && ((startX > otherBoxStartX && endX < otherBoxEndX) || (startZ > otherBoxStartZ && endZ < otherBoxEndZ))) partOfOtherFace = true
                        if (startZ == endZ && endZ == otherBoxStartZ && ((startX > otherBoxStartX && endX < otherBoxEndX) || (startY > otherBoxStartY && endY < otherBoxEndY))) partOfOtherFace = true
                    }
                }

                var q = (endX - startX).toFloat()
                var r = (endY - startY).toFloat()
                var s = (endZ - startZ).toFloat()
                val t = Mth.sqrt(q * q + r * r + s * s)
                q /= t
                r /= t
                s /= t
                val isOnSideFacingPlayer = q > 1000 // dummy condition for formatting 😅
                        || ((q == 0.0F) && ((startX != boxStartX && relX + startX < 0) || (endX != boxEndX && relX + endX > 0)))
                        || ((r == 0.0F) && ((startY != boxStartY && relY + startY < 0) || (endY != boxEndY && relY + endY > 0)))
                        || ((s == 0.0F) && ((startZ != boxStartZ && relZ + startZ < 0) || (endZ != boxEndZ && relZ + endZ > 0)))
                if (log) LOGGER.debug("[OUT-X] s={} z={} startZ={} endZ={} ", s, relZ, startZ, endZ)

                val occlusionAlpha = blinkAlpha * if (isOnSideFacingPlayer && !partOfOtherFace) 1f else 0.3f
                vertexConsumer.vertex(
                    pose.pose(),
                    (startX + relX).toFloat(),
                    (startY + relY).toFloat(),
                    (startZ + relZ).toFloat()
                )
                    .color(config.red, config.green, config.blue, occlusionAlpha)
                    .normal(pose.normal(), q, r, s).endVertex()
                vertexConsumer.vertex(
                    pose.pose(),
                    (endX + relX).toFloat(),
                    (endY + relY).toFloat(),
                    (endZ + relZ).toFloat()
                )
                    .color(config.red, config.green, config.blue, occlusionAlpha)
                    .normal(pose.normal(), q, r, s).endVertex()
                //if (log) LOGGER.debug(
                //    "[OUT-LINE] startX={}, startY={}, startZ={}, endX={}, endY={}, endZ={} --- q={}, r={}, s={}, t={} ==> {}",
                //    startX, startY, startZ, endX, endY, endZ, q, r, s, t, isOnSideFacingPlayer
                //)
            }

            //drawLine(boxStartX, boxStartY, boxStartZ, boxEndX, boxEndY, boxEndZ);
            drawLine(boxStartX, boxStartY, boxStartZ, boxStartX, boxStartY, boxEndZ);
            drawLine(boxStartX, boxStartY, boxStartZ, boxStartX, boxEndY, boxStartZ);
            drawLine(boxStartX, boxEndY, boxStartZ, boxStartX, boxEndY, boxEndZ);
            drawLine(boxStartX, boxStartY, boxEndZ, boxStartX, boxEndY, boxEndZ);
            drawLine(boxEndX, boxStartY, boxStartZ, boxEndX, boxStartY, boxEndZ);
            drawLine(boxEndX, boxStartY, boxStartZ, boxEndX, boxEndY, boxStartZ);
            drawLine(boxEndX, boxEndY, boxStartZ, boxEndX, boxEndY, boxEndZ);
            drawLine(boxEndX, boxStartY, boxEndZ, boxEndX, boxEndY, boxEndZ);
            drawLine(boxStartX, boxStartY, boxStartZ, boxEndX, boxStartY, boxStartZ);
            drawLine(boxStartX, boxStartY, boxEndZ, boxEndX, boxStartY, boxEndZ);
            drawLine(boxStartX, boxEndY, boxStartZ, boxEndX, boxEndY, boxStartZ);
            drawLine(boxStartX, boxEndY, boxEndZ, boxEndX, boxEndY, boxEndZ);
        }
        poseStack.popPose()
    }

    private fun drawBlinkingBlock(
        poseStack: PoseStack,
        bufferSource: MultiBufferSource.BufferSource,
        voxelShape: VoxelShape,
        camX: Double,
        camY: Double,
        camZ: Double,
        blockPos: BlockPos,
        config: CSBConfig
    ) {
        val vertexConsumer: VertexConsumer
        val poseStack2: PoseStack
        val tesselator = Tesselator.getInstance()
        //val builder = RenderType.CompositeState.builder()
        //builder.setShaderState(/*RENDERTYPE_TRANSLUCENT_MOVING_BLOCK_SHADER*/RenderStateShard.RENDERTYPE_SOLID_SHADER)
        ////builder.setLineState(new LineStateShard(OptionalDouble.of(config.getThickness())));
        ////builder.setLayeringState(VIEW_OFFSET_Z_LAYERING);
        //builder.setTransparencyState(RenderStateShard.TRANSLUCENT_TRANSPARENCY)
        ////builder.setOutputState(ITEM_ENTITY_TARGET);
        //if (config.disableDepthBuffer) builder.setDepthTestState(RenderStateShard.NO_DEPTH_TEST)
        //////if (!config.getDisableDepthBuffer()) builder.setDepthTestState(LEQUAL_DEPTH_TEST);
        //if (!config.disableDepthBuffer) builder.setWriteMaskState(RenderStateShard.COLOR_DEPTH_WRITE)
        //if (1 < 2) builder.setCullState(RenderStateShard.NO_CULL)
        //val compositeState = builder.createCompositeState(false)
        //val renderType = RenderType.create(
        //    "csb-box",
        //    DefaultVertexFormat.POSITION_COLOR,
        //    VertexFormat.Mode.TRIANGLE_STRIP,
        //    256,
        //    compositeState
        //)
        //vertexConsumer = bufferSource.getBuffer(renderType)
        //vertexConsumer = bufferSource.getBuffer(RenderType.lightning());

        poseStack2 = RenderSystem.getModelViewStack()
        poseStack2.pushPose()
        poseStack2.mulPoseMatrix(poseStack.last().pose())
        RenderSystem.applyModelViewMatrix()
        RenderSystem.enableBlend()
        //RenderSystem.blendEquation(32774);
        //RenderSystem.blendFunc(770, 1);
        //RenderSystem.disableBlend();
        //RenderSystem.defaultBlendFunc(); //== GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO
        //RenderSystem.blendFunc(GlStateManager.SourceFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        //RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        //RenderSystem.blendFuncSeparate(
        //        GlStateManager.SourceFactor.SRC_COLOR,
        //        GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA,
        //        GlStateManager.SourceFactor.ONE,
        //        GlStateManager.DestFactor.DST_ALPHA
        //);
        //RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        //RenderSystem.lineWidth(2.0F);
        RenderSystem.disableTexture()
        //RenderSystem.setShaderColor(1, 0 ,0, 1f);
        //RenderSystem.depthMask(true);
        RenderSystem.depthMask(false)
        RenderSystem.enableDepthTest()
        //RenderSystem.disableDepthTest();
        //RenderSystem.lineWidth(2.0F);
        RenderSystem.disableTexture()
        RenderSystem.setShader { GameRenderer.getPositionColorShader() }
        vertexConsumer = tesselator.builder
        //val pose = poseStack.last()

        val sin = Math.sin(System.currentTimeMillis() / 70.0 * config.blinkSpeed)
        val blinkingMultiplier = if (config.blinkSpeed > 0) 1 - Math.abs(sin).toFloat() else 1.0f
        val blinkAlpha = config.blockAlpha - config.blockAlpha * config.blinkAlpha * blinkingMultiplier
        val red = config.red
        val green = config.green
        val blue = config.blue

        // Inspired by: net.minecraft.client.renderer.debug.SolidFaceRenderer.render
        for (aABB in voxelShape.toAabbs()) {
            val aABB2 = aABB.move(blockPos).inflate(0.002).move(-camX, -camY, -camZ)
            box(log, camX, camY, camZ, aABB, aABB2, vertexConsumer, red, green, blue, blinkAlpha, tesselator)

            //break
        }
        poseStack2.popPose()
    }

    fun box(
        log: Boolean,
        camX: Double,
        camY: Double,
        camZ: Double,
        aABB: AABB?,
        aABB2: AABB,
        vertexConsumer: BufferBuilder,
        red: Float,
        green: Float,
        blue: Float,
        blinkAlpha: Float,
        tesselator: Tesselator
    ) {
        if (log) LOGGER.debug("[BOX] camX={}, camY={}, camZ={}, aABB={} aab2={}", camX, camY, camZ, aABB, aABB2)
        val g = aABB2.minX
        val h = aABB2.minY
        val i = aABB2.minZ
        val j = aABB2.maxX
        val k = aABB2.maxY
        val l = aABB2.maxZ

        vertexConsumer.begin(VertexFormat.Mode.TRIANGLE_STRIP, DefaultVertexFormat.POSITION_COLOR)
        vertexConsumer.vertex(g, h, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, h, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, k, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, k, l).color(red, green, blue, blinkAlpha).endVertex()
        tesselator.end()

        vertexConsumer.begin(VertexFormat.Mode.TRIANGLE_STRIP, DefaultVertexFormat.POSITION_COLOR)
        vertexConsumer.vertex(g, k, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, h, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, k, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, h, l).color(red, green, blue, blinkAlpha).endVertex()
        tesselator.end()

        vertexConsumer.begin(VertexFormat.Mode.TRIANGLE_STRIP, DefaultVertexFormat.POSITION_COLOR)
        vertexConsumer.vertex(j, h, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, h, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, k, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, k, i).color(red, green, blue, blinkAlpha).endVertex()
        tesselator.end()

        vertexConsumer.begin(VertexFormat.Mode.TRIANGLE_STRIP, DefaultVertexFormat.POSITION_COLOR)
        vertexConsumer.vertex(j, k, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, h, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, k, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, h, i).color(red, green, blue, blinkAlpha).endVertex()
        tesselator.end()

        vertexConsumer.begin(VertexFormat.Mode.TRIANGLE_STRIP, DefaultVertexFormat.POSITION_COLOR)
        vertexConsumer.vertex(g, h, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, h, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, h, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, h, l).color(red, green, blue, blinkAlpha).endVertex()
        tesselator.end()

        vertexConsumer.begin(VertexFormat.Mode.TRIANGLE_STRIP, DefaultVertexFormat.POSITION_COLOR)
        vertexConsumer.vertex(g, k, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(g, k, l).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, k, i).color(red, green, blue, blinkAlpha).endVertex()
        vertexConsumer.vertex(j, k, l).color(red, green, blue, blinkAlpha).endVertex()
        tesselator.end()
    }

    fun adjustShapeByLinkedBlocks(
        world: ClientLevel,
        blockState: BlockState,
        blockPos: BlockPos,
        shape: VoxelShape
    ): VoxelShape {
        try {
            if (log) LOGGER.debug("Link? {} , {}", blockState, shape)
            val booleanOp = BooleanOp.OR
            if (blockState.block is ChestBlock) {
                // Chests
                val block: Block = blockState.block
                val facing: Direction = ChestBlock.getConnectedDirection(blockState)
                val anotherChestState = world.getBlockState(blockPos.relative(facing, 1))
                if (anotherChestState.block === block
                    && blockPos.relative(facing, 1).relative(ChestBlock.getConnectedDirection(anotherChestState))
                        .equals(blockPos)
                )
                    return Shapes.join(
                        shape,
                        anotherChestState.getShape(world, blockPos)
                            .move(facing.stepX.toDouble(), facing.stepY.toDouble(), facing.stepZ.toDouble()),
                        booleanOp
                    )
            } else if (blockState.block is DoorBlock) {
                // Doors
                val block: Block = blockState.block
                if (world.getBlockState(blockPos.above(1)).block === block) {
                    val otherState = world.getBlockState(blockPos.above(1))
                    if (otherState.getValue(DoorBlock.POWERED)
                            .equals(blockState.getValue(DoorBlock.POWERED)) && otherState.getValue(
                            DoorBlock.FACING
                        ).equals(blockState.getValue(DoorBlock.FACING)) && otherState.getValue(DoorBlock.HINGE)
                            .equals(blockState.getValue(DoorBlock.HINGE))
                    ) {
                        return Shapes.join(
                            shape,
                            otherState.getShape(world, blockPos).move(0.0, 1.0, 0.0),
                            booleanOp
                        )
                    }
                }
                if (world.getBlockState(blockPos.below(1)).block === block) {
                    val otherState = world.getBlockState(blockPos.below(1))
                    if (otherState.getValue(DoorBlock.POWERED)
                            .equals(blockState.getValue(DoorBlock.POWERED)) && otherState.getValue(
                            DoorBlock.FACING
                        ).equals(blockState.getValue(DoorBlock.FACING)) && otherState.getValue(DoorBlock.HINGE)
                            .equals(blockState.getValue(DoorBlock.HINGE))
                    ) return Shapes.join(
                        shape,
                        otherState.getShape(world, blockPos).move(0.0, -1.0, 0.0),
                        booleanOp
                    )
                }
            } else if (blockState.block is BedBlock) {
                // Beds
                val block: Block = blockState.block
                var direction: Direction = blockState.getValue(HorizontalDirectionalBlock.FACING)
                var otherState = world.getBlockState(blockPos.relative(direction))
                if (blockState.getValue(BedBlock.PART).equals(BedPart.FOOT) && otherState.block === block) {
                    if (otherState.getValue(BedBlock.PART).equals(BedPart.HEAD)) return Shapes.join(
                        shape, otherState.getShape(world, blockPos).move(
                            direction.stepX.toDouble(), direction.stepY.toDouble(), direction.stepZ.toDouble()
                        ), booleanOp
                    )
                }
                otherState = world.getBlockState(blockPos.relative(direction.getOpposite()))
                direction = direction.getOpposite()
                if (blockState.getValue(BedBlock.PART).equals(BedPart.HEAD) && otherState.block === block) {
                    if (otherState.getValue(BedBlock.PART).equals(BedPart.FOOT)) return Shapes.join(
                        shape,
                        otherState.getShape(world, blockPos)
                            .move(direction.stepX.toDouble(), direction.stepY.toDouble(), direction.stepZ.toDouble()),
                        booleanOp
                    )
                }
            } else if (blockState.block is PistonBaseBlock && blockState.getValue(PistonBaseBlock.EXTENDED)) {
                // Piston Base
                val block: Block = blockState.block
                val direction: Direction = blockState.getValue(DirectionalBlock.FACING)
                val otherState = world.getBlockState(blockPos.relative(direction))
                if (otherState.getValue(PistonHeadBlock.TYPE).equals(
                        if (block === Blocks.PISTON) PistonType.DEFAULT else PistonType.STICKY
                    ) && direction.equals(otherState.getValue(DirectionalBlock.FACING))
                ) return Shapes.join(
                    shape,
                    otherState.getShape(world, blockPos)
                        .move(direction.stepX.toDouble(), direction.stepY.toDouble(), direction.stepZ.toDouble()),
                    booleanOp
                )
            } else if (blockState.block is PistonHeadBlock) {
                // Piston Arm
                val block: Block = blockState.block
                val direction: Direction = blockState.getValue(DirectionalBlock.FACING)
                val otherState = world.getBlockState(blockPos.relative(direction.getOpposite()))
                if (otherState.block is PistonBaseBlock && direction === otherState.getValue(DirectionalBlock.FACING) && otherState.getValue(
                        PistonBaseBlock.EXTENDED
                    )
                ) return Shapes.join(
                    shape, otherState.getShape(world, blockPos.relative(direction.getOpposite())).move(
                        direction.getOpposite().stepX.toDouble(),
                        direction.getOpposite().stepY.toDouble(),
                        direction.getOpposite().stepZ.toDouble()
                    ), booleanOp
                )
            }
        } catch (exception: Exception) {
            if (log) LOGGER.debug("Ignoring link exception: {}", exception)
        }
        return shape
    }
}