package io.txlab.mods.customselectionbox

import com.mojang.blaze3d.vertex.PoseStack
import io.txlab.mods.customselectionbox.CustomSelectionBox.LOGGER
import io.txlab.mods.customselectionbox.config.ConfigPersistence
import io.txlab.mods.customselectionbox.mixin.accessors.ScreenAccessor
import net.minecraft.client.gui.components.Button
import net.minecraft.client.gui.screens.Screen
import net.minecraft.network.chat.Component

class CSBSettingsScreen(var parent: Screen) : Screen(Component.translatable("customselectionbox.options.title")) {
    @Suppress("UNUSED_CHANGED_VALUE")
    public override fun init() {
        val thisAcc = this as ScreenAccessor
        val config = CustomSelectionBox.CONFIG
        val sliderHeight = 15
        val gap = 4

        // LEFT //
        var yStart = height / 2 - 62
        var i = 0
        addRenderableWidget(CSBSlider("red", gap, yStart + (sliderHeight + gap) * i++))
        addRenderableWidget(CSBSlider("green", gap, yStart + (sliderHeight + gap) * i++))
        addRenderableWidget(CSBSlider("blue", gap, yStart + (sliderHeight + gap) * i++))
        addRenderableWidget(CSBSlider("alpha", gap, yStart + (sliderHeight + gap) * i++))
        addRenderableWidget(CSBSlider("thickness", gap, yStart + (sliderHeight + gap) * i++))
        val makeDepthText = { Component.translatable("customselectionbox.options.disableDepth", if (config.disableDepthBuffer) "ON" else "OFF") }
        addRenderableWidget(Button(
            gap, yStart + (sliderHeight + gap) * i++, 150, 20, makeDepthText()
        ) {
            config.disableDepthBuffer = !config.disableDepthBuffer
            it.message = makeDepthText()
        })

        // RIGHT //
        yStart = height / 2 - 52
        i = 0
        val makeBreakAnimationText = { Component.translatable("customselectionbox.options.breakAnimation", "TODO") }
        addRenderableWidget(Button(
            width - 150 - gap, yStart + (sliderHeight + gap) * i++, 150, 20, makeBreakAnimationText()
        ) {
            //config.breakAnimation = BREAK_ANIMATION.values()[1]
            it.message = makeBreakAnimationText()
        })
        yStart += 5 // button is 5 higher

        addRenderableWidget(CSBSlider("blockAlpha", width - 150 - gap, yStart + (sliderHeight + gap) * i++))
        addRenderableWidget(CSBSlider("blinkAlpha", width - 150 - gap, yStart + (sliderHeight + gap) * i++))
        addRenderableWidget(CSBSlider("blinkSpeed", width - 150 - gap, yStart + (sliderHeight + gap) * i++))

        val makeLinkBlockText = { Component.translatable("customselectionbox.options.linkBlocks", if (config.disableDepthBuffer) "ON" else "OFF")  }
        addRenderableWidget(Button(
            width - 150 - gap, yStart + (sliderHeight + gap) * i++, 150, 20, makeLinkBlockText()
        ) {
            config.linkBlocks = !config.linkBlocks
            it.message = makeLinkBlockText()
        })

        // BOTTOM //
        addRenderableWidget(Button(width / 2 - 100, height - 48, 200, 20, Component.literal("Done")) {
            LOGGER.info("Done clicked, saving {}", config)
            ConfigPersistence.save(config)
            LOGGER.debug("Done clicked, setting screen back to {}", this.parent)
            getMinecraft().setScreen(this.parent)
        })
        addRenderableWidget(Button(width / 2 - 100, height - 24, 95, 20, Component.literal("CSB defaults")) {
            config.defaultsCSB()
            rebuildWidgets()
        })
        addRenderableWidget(Button(width / 2 + 5, height - 24, 95, 20, Component.literal("MC defaults")) {
            config.defaultsMC()
            rebuildWidgets()
        })
    }

    //protected fun keyTyped(par1: Char, par2: Int) {
    //    if (par2 == 1) {
    //        CustomSelectionBox.CONFIG.save()
    //        (this as ScreenAccessor).minecraft.setScreen(null)
    //    }
    //}

    override fun renderBackground(poseStack: PoseStack, i: Int) {
        if ((this as ScreenAccessor).minecraft.level != null) {
            // top
            this.fillGradient(poseStack, 0, 0, width, 48 - 4, -1072689136, -804253680)
            // left
            this.fillGradient(poseStack, 0, height / 2 - 67, 158, height / 2 + 59, -1072689136, -804253680)
            // right
            this.fillGradient(
                poseStack,
                width - 158,
                height / 2 - 56,
                width,
                height / 2 + 52,
                -1072689136,
                -804253680
            )
            // bottom
            this.fillGradient(poseStack, 0, height - 48 - 4, width, height, -1072689136, -804253680)
        } else {
            renderDirtBackground(i)
        }
    }

    override fun render(poseStack: PoseStack, i: Int, j: Int, f: Float) {
        //drawString(this.fontRendererObj, "Blink:", this.width - 153, this.height / 2 + 0, 16777215);
        renderBackground(poseStack)
        drawCenteredString(poseStack, font, this.title, width / 2, (height - (height + 4 - 48)) / 2 - 4, 16777215)
        super.render(poseStack, i, j, f)
    }
}