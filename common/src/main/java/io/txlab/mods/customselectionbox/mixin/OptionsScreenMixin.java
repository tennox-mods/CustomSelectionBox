package io.txlab.mods.customselectionbox.mixin;

import com.google.common.collect.ImmutableMap;
import com.mojang.blaze3d.vertex.PoseStack;
import io.txlab.mods.customselectionbox.CSBSettingsScreen;
import io.txlab.mods.customselectionbox.CustomSelectionBox;
import io.txlab.mods.customselectionbox.mixin.accessors.AbstractSelectionListAccessor;
import io.txlab.mods.customselectionbox.mixin.accessors.EntryAccessor;
import io.txlab.mods.customselectionbox.mixin.accessors.VideoSettingsScreenAccessor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.OptionInstance;
import net.minecraft.client.gui.components.AbstractSelectionList;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.components.Button;
import net.minecraft.client.gui.components.OptionsList;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.VideoSettingsScreen;
import net.minecraft.network.chat.Component;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(VideoSettingsScreen.class)
public class OptionsScreenMixin {
    @Inject(method = "init", at = @At("TAIL"))
    public void onInit(CallbackInfo ci) {
        if (CustomSelectionBox.disableVideoScreenButton) {
            CustomSelectionBox.LOGGER.info("CSB NOT mixing in OptionsScreen");
            return;
        }
        CustomSelectionBox.LOGGER.info("CSB mixing in OptionsScreen");
        VideoSettingsScreenAccessor screen = (VideoSettingsScreenAccessor) (Object) this;
        //		if (screen.getMinecraft().level == null) {
        //			CustomSelectionBox.LOGGER.info("CSB skipping OptionsScreen, no level is loaded");
        //			return;
        //		}

        Button button = new Button(
                screen.getWidth() / 2 + 5,
                screen.getHeight() / 6 + 120 - 6,
                150,
                20,
                Component.translatable("customselectionbox.options.open-config"),
                but -> screen.getMinecraft().setScreen(new CSBSettingsScreen((Screen) (Object) screen))
        );
        OptionInstance optionInstance = new OptionInstance(
                "customselectionbox.options.open-config",
                OptionInstance.noTooltip(),
                (component, val) -> Component.literal(""),
                new OptionInstance.ClampingLazyMaxIntRange(0, () -> {
                    Minecraft minecraftx = Minecraft.getInstance();
                    return !minecraftx.isRunning() ? 2147483646 : minecraftx.getWindow().calculateScale(0, minecraftx.isEnforceUnicode());
                }),
                0,
                val -> {
                    screen.getMinecraft().setScreen(new CSBSettingsScreen((Screen) (Object) screen));
                }
        );
        ((AbstractSelectionListAccessor/*<AbstractSelectionList.Entry>*/) screen.getList()).callAddEntry(EntryAccessor.createEntry(ImmutableMap.of(optionInstance, button)));
        /*.addSmall(
                new OptionInstance[]{new OptionInstance(
                        "customselectionbox.options.open-config",
                        OptionInstance.noTooltip(),
                        (component, val) -> Component.literal(""),
                        new OptionInstance.ClampingLazyMaxIntRange(0, () -> {
                            Minecraft minecraftx = Minecraft.getInstance();
                            return !minecraftx.isRunning() ? 2147483646 : minecraftx.getWindow().calculateScale(0, minecraftx.isEnforceUnicode());
                        }),
                        0,
                        val -> {
                            screen.getMinecraft().setScreen(new CSBSettingsScreen((Screen) (Object) screen));
                        }
                ) {}}
        );*/


		/*screen.getList().addSmall(
				new OptionInstance(
						"customselectionbox.options.open-config",
						OptionInstance.noTooltip(),
						(component, val) -> Component.literal("") ,
						new Object() {
							Function<OptionInstance<Boolean>, AbstractWidget> createButton(OptionInstance.TooltipSupplier<Boolean> tooltipSupplier, Options options, int i, int j, int k) {
								return null;
							}

							Optional<Boolean> validateValue(Boolean object) {
								return Optional.of(false);
							}

							Codec<Boolean> codec() {
								return null;
							}
						},
						0,
						integer -> {
						}
				)*/
    }

}
