package io.txlab.mods.customselectionbox.mixin.accessors;

import io.txlab.mods.customselectionbox.mixin.accessors.ScreenAccessor;
import net.minecraft.client.gui.components.OptionsList;
import net.minecraft.client.gui.screens.VideoSettingsScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(VideoSettingsScreen.class)
public interface VideoSettingsScreenAccessor extends ScreenAccessor {
    @Accessor
    OptionsList getList();
}
