package io.txlab.mods.customselectionbox.mixin.accessors;

import net.minecraft.client.gui.components.AbstractSelectionList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(AbstractSelectionList.class)
public interface AbstractSelectionListAccessor/*<E extends AbstractSelectionList.Entry<E>>*/ {
    @Invoker
    int callAddEntry(AbstractSelectionList.Entry entry);
}
