package io.txlab.mods.customselectionbox.mixin.accessors;

import net.minecraft.client.OptionInstance;
import net.minecraft.client.gui.components.AbstractWidget;
import net.minecraft.client.gui.components.OptionsList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.Map;

@Mixin(OptionsList.Entry.class)
public interface EntryAccessor {
    @Invoker("<init>")
    static OptionsList.Entry createEntry(Map<OptionInstance<?>, AbstractWidget> map) {
        throw new UnsupportedOperationException();
    }
}
