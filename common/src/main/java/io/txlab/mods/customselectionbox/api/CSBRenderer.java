package io.txlab.mods.customselectionbox.api;

import com.mojang.blaze3d.vertex.PoseStack;
import io.txlab.mods.customselectionbox.CSBConfig;
import io.txlab.mods.customselectionbox.CustomSelectionBox;
import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public interface CSBRenderer {
    /**
     * The priority of this renderer, smaller priorities are called first, and once it has been rendered,
     * all priorities that has a bigger priority in number will be ignored.
     *
     * @return the priority of this renderer in double.
     */
    default float getPriority() {
        return 100;
    }
    
    default Minecraft getClient() {
        return Minecraft.getInstance();
    }

    //default float getOutlineThickness() {
    //    return CustomSelectionBox.CONFIG.getThickness() * Math.max(2.5F, (float)Minecraft.getInstance().getWindow().getWidth() / 1920.0F * 2.5F);
    //}
    //default int getOutlineColor() {
    //    return (((int) (getOutlineAlpha() * 255)) & 255) << 24 | (((int) (getOutlineRed() * 255)) & 255) << 16 | (((int) (getOutlineGreen() * 255)) & 255) << 8 | (((int) (getOutlineBlue() * 255)) & 255);
    //}
    //default float getOutlineRed() {
    //    return CustomSelectionBox.CONFIG.getRed();
    //}
    //default float getOutlineGreen() {
    //    return CustomSelectionBox.CONFIG.getGreen();
    //}
    //default float getOutlineBlue() {
    //    return CustomSelectionBox.CONFIG.getBlue();
    //}
    //default float getOutlineAlpha() {
    //    return CustomSelectionBox.CONFIG.getAlpha();
    //}
    //default int getInnerColor() {
    //
    //
    //    return (((int) (getInnerAlpha() * 255)) & 255) << 24 | (((int) (getInnerRed() * 255)) & 255) << 16 | (((int) (getInnerGreen() * 255)) & 255) << 8 | (((int) (getInnerBlue() * 255)) & 255);
    //}
    //default float getInnerRed() {
    //    return CustomSelectionBox.CONFIG.getRed();
    //}
    //default float getInnerGreen() {
    //    return CustomSelectionBox.CONFIG.getGreen();
    //}
    //default float getInnerBlue() {
    //    return CustomSelectionBox.CONFIG.getBlue();
    //}
    //default float getInnerAlpha() {
    //    return CustomSelectionBox.CONFIG.getAlpha() * CustomSelectionBox.CONFIG.getBlockAlpha();
    //}
    
    boolean render(PoseStack poseStack, MultiBufferSource.BufferSource bufferSource, ClientLevel world, Entity entity, double camX, double camY, double camZ, BlockPos blockPos, BlockState blockState, CSBConfig config);
}
