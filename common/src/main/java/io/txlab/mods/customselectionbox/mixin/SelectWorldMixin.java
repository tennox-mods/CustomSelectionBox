package io.txlab.mods.customselectionbox.mixin;

import net.minecraft.client.gui.screens.worldselection.WorldSelectionList;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = WorldSelectionList.WorldListEntry.class)
public abstract class SelectWorldMixin {
    @Shadow
    public abstract void joinWorld();

    @Inject(at = @At("TAIL"), method = "render")
    private void render(CallbackInfo info) {
        if (System.getenv("DEV") != null && System.getenv("DEV").contentEquals("1")) {
            this.joinWorld();
        }
    }
}
