package io.txlab.mods.customselectionbox.mixin;

import io.txlab.mods.customselectionbox.mixin.accessors.ScreenAccessor;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraft.client.gui.screens.worldselection.SelectWorldScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(TitleScreen.class)
public class MixinTitleScreen {
    @Inject(at = @At("HEAD"), method = "init()V")
    private void init(CallbackInfo info) {
        if (System.getenv("DEV") != null && System.getenv("DEV").contentEquals("1")) {
            System.out.println("6 Hello from example architectury common mixin!");
            ((ScreenAccessor) (Object) this).getMinecraft().setScreen(new SelectWorldScreen((Screen) (Object) this));
        }
    }
}