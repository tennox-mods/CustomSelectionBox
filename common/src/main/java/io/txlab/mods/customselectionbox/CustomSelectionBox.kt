package io.txlab.mods.customselectionbox

import io.txlab.mods.customselectionbox.api.CSBRenderer
import io.txlab.mods.customselectionbox.config.ConfigPersistence
import io.txlab.mods.customselectionbox.renderer.CSBDefaultRenderer
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object CustomSelectionBox {
    const val MOD_ID = "customselectionbox"

    @JvmField
    var CONFIG: CSBConfig = CSBConfig()
    @JvmField
    val LOGGER: Logger = LoggerFactory.getLogger(CustomSelectionBox::class.java)
    @JvmField
    var RENDERERS = ArrayList<CSBRenderer>()

    @JvmField
    var disableVideoScreenButton = false

    // We can use this if we don't want to use DeferredRegister
    //val REGISTRIES: Supplier<Registries> = Suppliers.memoize { Registries.get(MOD_ID) }

    @JvmStatic
    fun init() {
        //if (System.getenv("DEV") != null && System.getenv("DEV").contentEquals("1")) (LOGGER as Log4jLogger).setLevel(nnope)
        //System.setProperty("log4j.configurationFile", "classpath:log4j2.fabric.xml");
        //org.apache.log4j.Logger.getRootLogger();
        //logger4j.setLevel(org.apache.log4j.Level.toLevel("DEBUG"));
        CONFIG = ConfigPersistence.load()
        //LOGGER.info(ExampleExpectPlatform.getConfigDirectory().toAbsolutePath().normalize().toString())
        LOGGER.info("CustomSelectionBox loaded (r={},g={},thk={})", CONFIG.red, CONFIG.green, CONFIG.thickness)
        LOGGER.info("CustomSelectionBox LOG {}, {}", LOGGER.name, LOGGER::class.java)
        LOGGER.debug("CustomSelectionBox DEBUG")

        RENDERERS.add(CSBDefaultRenderer())
    }
}