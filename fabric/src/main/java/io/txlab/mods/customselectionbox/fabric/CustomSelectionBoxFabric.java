package io.txlab.mods.customselectionbox.fabric;

 import io.txlab.mods.customselectionbox.fabriclike.CustomSelectionBoxFabricLike;
import net.fabricmc.api.ModInitializer;

public class CustomSelectionBoxFabric implements ModInitializer {
    @Override
    public void onInitialize() {
        CustomSelectionBoxFabricLike.init();
    }
}
