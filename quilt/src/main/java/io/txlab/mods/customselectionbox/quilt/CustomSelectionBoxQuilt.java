package io.txlab.mods.customselectionbox.quilt;

import io.txlab.mods.customselectionbox.fabriclike.CustomSelectionBoxFabricLike;
import org.quiltmc.loader.api.ModContainer;
import org.quiltmc.qsl.base.api.entrypoint.ModInitializer;

public class CustomSelectionBoxQuilt implements ModInitializer {
    @Override
    public void onInitialize(ModContainer mod) {
        CustomSelectionBoxFabricLike.init();
    }
}
