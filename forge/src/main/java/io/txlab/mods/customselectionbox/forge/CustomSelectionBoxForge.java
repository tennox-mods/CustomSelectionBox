package io.txlab.mods.customselectionbox.forge;

import dev.architectury.platform.forge.EventBuses;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import io.txlab.mods.customselectionbox.CustomSelectionBox;

@Mod(CustomSelectionBox.MOD_ID)
public class CustomSelectionBoxForge {
    public CustomSelectionBoxForge() {
        // Submit our event bus to let architectury register our content on the right time
        EventBuses.registerModEventBus(CustomSelectionBox.MOD_ID, FMLJavaModLoadingContext.get().getModEventBus());
        CustomSelectionBox.init();
    }
}
