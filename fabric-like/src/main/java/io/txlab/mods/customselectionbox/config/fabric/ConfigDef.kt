package io.txlab.mods.customselectionbox.config.fabric

import io.github.redstoneparadox.paradoxconfig.config.ConfigCategory
import io.txlab.mods.customselectionbox.CSBConfig
import io.txlab.mods.customselectionbox.CustomSelectionBox

val defaults = CSBConfig()

object ConfigDef : ConfigCategory(CustomSelectionBox.MOD_ID + ".json") {
    var red by option(defaults.red, "red", "Red")
    var green by option(defaults.green, "green", "Green")
    var blue by option(defaults.blue, "blue", "Blue")
    var alpha by option(defaults.alpha, "alpha", "Alpha")
    var thickness by option(defaults.thickness, "thickness", "Line Thickness")

    var blinkAlpha by option(defaults.blinkAlpha, "blinkAlpha", "Blink Alpha")
    var blinkSpeed by option(defaults.blinkSpeed, "blinkSpeed", "Blink Speed")
    var blockAlpha by option(defaults.blockAlpha, "blockAlpha", "Block fill Alpha")

    var linkBlocks by option(defaults.linkBlocks, "linkBlocks", "Merge multi-block bounding boxes (i.e. chest, door)")
    var disableDepthBuffer by option(defaults.disableDepthBuffer, "disableDepthBuffer", "Disable Depth Buffer")
    var breakAnimation by option(defaults.breakAnimation, "breakAnimation", "Break animation")

    // --- Further examples: https://github.com/RedstoneParadox/ParadoxConfig/blob/master/src/testmod/java/io/github/redstoneparadox/paradoxconfig/GsonTestConfig.kt
}