package io.txlab.mods.customselectionbox.config.fabric;

import io.github.redstoneparadox.paradoxconfig.codec.ConfigCodec
import io.txlab.mods.customselectionbox.CSBConfig
import io.txlab.mods.customselectionbox.CustomSelectionBox
import io.txlab.mods.customselectionbox.CustomSelectionBox.LOGGER
import net.fabricmc.loader.api.FabricLoader
import java.io.File

class ConfigPersistenceImpl {
    companion object {
        @JvmStatic
        fun load(): CSBConfig {
            return CSBConfig().apply {
                defaultsMC()
                red = ConfigDef.red
                green = ConfigDef.green
                blue = ConfigDef.blue
                alpha = ConfigDef.alpha
                thickness = ConfigDef.thickness
                blinkAlpha = ConfigDef.blinkAlpha
                blinkSpeed = ConfigDef.blinkSpeed
                blockAlpha = ConfigDef.blockAlpha
                linkBlocks = ConfigDef.linkBlocks
                disableDepthBuffer = ConfigDef.disableDepthBuffer
                breakAnimation = ConfigDef.breakAnimation
            }
        }
        @JvmStatic
        fun save(config: CSBConfig) {
            LOGGER.info("Saving config: {}", config)
            ConfigDef.red = config.red
            ConfigDef.green = config.green
            ConfigDef.blue = config.blue
            ConfigDef.alpha = config.alpha
            ConfigDef.thickness = config.thickness
            ConfigDef.blinkAlpha = config.blinkAlpha
            ConfigDef.blinkSpeed = config.blinkSpeed
            ConfigDef.blockAlpha = config.blockAlpha
            ConfigDef.linkBlocks = config.linkBlocks
            ConfigDef.disableDepthBuffer = config.disableDepthBuffer
            ConfigDef.breakAnimation = config.breakAnimation


            // Copied from https://github.com/RedstoneParadox/ParadoxConfig/blob/master/src/main/java/io/github/redstoneparadox/paradoxconfig/ConfigManager.kt#L31
            val configCodec = ConfigCodec.getCodec("json")
            val file = File(FabricLoader.getInstance().configDirectory, "${CustomSelectionBox.MOD_ID }/${CustomSelectionBox.MOD_ID + ".json"}")
            LOGGER.info("Saving config def: {} with {} to {}", ConfigDef, configCodec, file)

            if (file.exists()) {
                LOGGER.info("Config file ${CustomSelectionBox.MOD_ID}:${CustomSelectionBox.MOD_ID + ".json"} was found; Overwriting it.")
                try {
                    file.writeText(configCodec.encode(ConfigDef))
                } catch (e: Exception) {
                    LOGGER.error("Could not write to config file ${CustomSelectionBox.MOD_ID}:${CustomSelectionBox.MOD_ID + ".json"} due to an exception.")
                    e.printStackTrace()
                }
            } else {
                LOGGER.info("Config file ${CustomSelectionBox.MOD_ID}:${CustomSelectionBox.MOD_ID + ".json"} was not found; a new one will be created.")
                try {
                    file.parentFile.mkdirs()
                    file.createNewFile()
                    file.writeText(configCodec.encode(ConfigDef))
                } catch (e: SecurityException) {
                    LOGGER.error("Could not create config file ${CustomSelectionBox.MOD_ID}:${CustomSelectionBox.MOD_ID + ".json"} due to security issues.")
                    e.printStackTrace()
                } catch (e: Exception) {
                    LOGGER.error("Could not create config file ${CustomSelectionBox.MOD_ID}:${CustomSelectionBox.MOD_ID + ".json"} due to an exception.")
                    e.printStackTrace()
                }
            }
        }
    }
}