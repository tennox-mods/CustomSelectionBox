package io.txlab.mods.customselectionbox.fabric

import com.terraformersmc.modmenu.api.ConfigScreenFactory
import com.terraformersmc.modmenu.api.ModMenuApi
import io.txlab.mods.customselectionbox.CSBSettingsScreen
import io.txlab.mods.customselectionbox.CustomSelectionBox

class CSBModMenu : ModMenuApi {
    override fun getModConfigScreenFactory(): ConfigScreenFactory<CSBSettingsScreen> {
        CustomSelectionBox.LOGGER.info("CSB ModMenuApi initializing")
        CustomSelectionBox.disableVideoScreenButton = true
        return ConfigScreenFactory<CSBSettingsScreen> { screen ->
            CSBSettingsScreen(screen)
        }
    }
}